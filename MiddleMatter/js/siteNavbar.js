function getCurFile() {
    var url = window.location.pathname;
    return url.substring(url.lastIndexOf('/')+1);
}

function makeNavbar()
{
    // That adds the navbar
    var nav = d3.select("body").insert("nav", ":first-child")
                .classed("top-bar", true)
                .attr("role", "navigation")
                .attr("data-topbar", true);

    nav.append("ul").classed("title-area", true)
       .append("li").classed("name", true)
       .append("h1")
       .append("a").attr("href", "#")
       .text("Parser visualization");


    var buttons = [
        { title:"Physics-based tree", subbuttons:[
            {title: "Naive", link:"naive.html" },
            {title: "Detached Terminals", link:"termDetach.html" },
            {title: "Layered", link:"layered.html" },
            {title: "Circles", link:"circled.html" },
            {title: "Tension Tweaking", link:"layerLinks.html" },
        ]},
        { title:"Animation", subbuttons:[ 
            {title: "Shunting Yard", link:"yard_animate.html"},
        ]},
        { title:"Other", subbuttons:[
            {title: "Shunting Yard", link:"index.html" },
        ]}
    ]; 

    var dropdownEnter = nav.append("section").classed("top-bar-section", true)
                          .selectAll("ul").data(buttons).enter()
                          .append("ul").classed("left", true)
                          .append("li").classed("has-dropdown", true);

    dropdownEnter.append("a").attr("href","#").text(function(d){return d.title;});

    dropdownEnter.append("ul").classed("dropdown", true)
                 .selectAll("li").data(function(d){return d.subbuttons;}).enter()
                 .append("li").classed("active", function(d){return d.link == getCurFile();})
                 .append("a").attr("href", function(d){return d.link;})
                             .text(function(d){return d.title;});
}
